
import  csv
import  numpy
import  sys

def  read_data_file(file_name):
    _result = {}
    with  open(file_name, 'r')  as  fr:
        _result = read_data_stream(fr)
    # End With
    return  _result
# End Def (read_data_file)

def  read_data_stream(fr):
    _result = {}
    reader = csv.reader(fr, delimiter='\t')
    header = next(reader)
    print(header)

    for  row  in  reader:
        if ( len(row) == 0 ) :
            continue
        # End IF

        _data = { k:row[i]  for i, k in enumerate(header) }
        _id   = _data['_id']
        _result[_id] = _data
    # End For (row)

    return  _result
# End Def (read_data_stream)

def  save_data_file(data, header, file_name):
    _result = None
    with  open(file_name, 'w')  as  fw:
        _result = save_data_stream(data, header, fw)
    # End With
    return  _result
# End Def (save_data_file)

def  save_data_stream(data, header, fw):
    fw.write('\t'.join(header) + '\n')
    for  i, key  in  enumerate(sorted(data)):
        _data = data[key]
        row_items = [ str(_data[k])  for k in header ]
        fw.write('\t'.join(row_items) + '\n')
    # End For (i, d)
    return  True
# End Def (save_data_stream)

def  update_working_data_from_user_judge(work, judge):
    _marker = { 'OO' : 10, 'O' : 5, 'A' : 1, 'X' : -1 }

    for  i, key  in  enumerate(judge):
        _jd = judge[key]
        _id = _jd['_id']
        _mk = _jd['judge']
        _lv = _marker[_mk]
        work[_id]['label:human'] = _lv
    # End For (i, jd)

    return  True
# End Def (update_working_data_from_user_judge)

def  get_primary_label(pd):
    for  _t  in  [ 'human', 'kw', 'lda' ] :
        _key = 'label:' + _t
        if ( _key in pd ):
            _value = pd[_key]
            if ( _value == 'None' ):
                continue
            # End If
            return   int(_value)
        # End If
    return  0
# End Def (get_primary_label)


def  prepare_learning_data(input_data, work_data):
    _feature = []
    _targets = []

    for  _id  in  sorted(work_data):
        _wdata = work_data[_id]
        _level = get_primary_label(_wdata)
        if ( _level == 0 ):
            continue
        # End If

        _text = input_data[_id]['wordvec']
        _fv = numpy.array(list(map(float,  _text.split(','))))
        print(_fv)

        if ( _level > 0 ):
            _count = _level
            _value = True
        else:
            _count = - _level
            _value = False
        # End If

        _feature.append(_fv)
        _targets.append(_value)
        for _i in range(1, _count):
            _feature.append(_fv)
            _targets.append(_value)
        # End For (_i)

        num_pos = _targets.count(True)
        num_neg = _targets.count(False)
        _debug = ('POS = ' + str(num_pos)
                  + ', NEG = ' + str(num_neg)
                  + ', SUM = ' + str(len(_targets))
                  + '\n')
        sys.stderr.write(_debug)
    # End For (_id)
# End Def (prepare_learning_data)

if ( __name__ == '__main__' ):
    input_data = read_data_file('testdata.txt')
    work_data = read_data_file('workdata.txt')
    judge_data = read_data_file('user_judge.txt')

    print(work_data)
    update_working_data_from_user_judge(work_data, judge_data)
    save_data_file(
        work_data,
        [ '_id', 'label:human', 'label:kw', 'label:lda' ],
        'workdata2.txt')
    print(work_data)

    prepare_learning_data(input_data, work_data)
# End If
