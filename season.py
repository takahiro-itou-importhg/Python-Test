import csv
import datetime as datetime  
import matplotlib.pyplot as plt
import pandas as pd
from statsmodels.tsa.seasonal import seasonal_decompose
#%matplotlib inline

filename = "input/tokyo2016.csv"
with open(filename, 'rt') as f:
    data = list(csv.reader(f))

headers = data.pop(0)
df = pd.DataFrame(data, columns=headers)

#dataFrame = pd.DataFrame(df['power'].values.astype(int), DatetimeIndex(start='2016-01-01', periods=len(df['power']), freq='D'))
dataFrame=pd.DataFrame(df['power'].values.astype(int))
ts = seasonal_decompose(dataFrame.values, freq=7)

plt.plot(ts.trend) # トレンド成分
plt.plot(ts.seasonal) # 季節成分
plt.plot(ts.resid) # ノイズ成分
plt.show()

