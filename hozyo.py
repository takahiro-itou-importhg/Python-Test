
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from statsmodels.tsa.api import VAR
import statsmodels.api as sm

def calc_kaisa(data):
    return (data - data.shift(1)).dropna()

def estimate_var(params, data):
    ret = params.intercept
    
    #print(data)
    input = [ y for y in zip(data) ]
    print(input)
    #for y in input:
    #    print(y)

    for coemat in params.coefs:
        print(coemat)
        work = [ coemat.dot(x) for x in input ]
        print(work)
    return  ret


if __name__ == "__main__":
    mdata=sm.datasets.macrodata.load().data
    mdata=mdata[['realgdp','realcons', 'realinv']]
    names=mdata.dtype.names
    data=mdata.view((float,3))
    model=VAR(data)
    res=model.fit(2)
    estimate_var(res, data)

    